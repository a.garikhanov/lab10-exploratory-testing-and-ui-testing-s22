import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

REDDIT_URL = "https://www.reddit.com"


@pytest.fixture()
def driver():
    driver = webdriver.Firefox()
    yield driver
    driver.quit()


def test_unauthorized_user_cannot_comment_posts(driver):
    driver.get(REDDIT_URL)

    # I use XPath here because reddit generates random classes and names for HTML elements
    first_post = driver.find_element(
        by=By.XPATH,
        value="/html/body/div[1]/div/div[2]/div[2]/div/div/div/div[2]/div[2]/div[1]/div[5]/div[1]/div/div/div[3]/div[2]/div[1]/a/div/h3",
    )
    first_post.click()
    caption = driver.find_element(
        by=By.XPATH,
        value="//*[contains(text(), 'Log in or sign up to leave a comment')]",
    )
    assert caption is not None


def test_search_subreddit(driver):
    driver.get(REDDIT_URL)

    search_bar = driver.find_element(by=By.XPATH, value='//*[@type="search"]')
    search_bar.click()
    search_bar.send_keys("r/movies")

    search_result = WebDriverWait(driver, 3).until(
        EC.presence_of_element_located((By.XPATH, '//*[@aria-label="r/movies"]'))
    )
    search_result.click()

    subreddit_title = WebDriverWait(driver, 3).until(
        EC.presence_of_element_located((By.TAG_NAME, "h1"))
    )
    assert "Movie" in subreddit_title.text


def test_help_page():
    driver.get(REDDIT_URL)

    help_link = driver.find_element(
        by=By.XPATH, value='//*[@href="https://www.reddithelp.com"]'
    )
    help_link.click()

    assert "reddithelp.com" in driver.current_url
